import { FormControl,FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UsersService } from "../users/users.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  invalid = false;
  loginform = new FormGroup({
	  user:new FormControl(),
	  password:new FormControl(),	    
  });


  login(){
    this.service.login(this.loginform.value).subscribe(responser=>{
      this.router.navigate(['/users']);
    },
    error=>{this.invalid= true;})
  }

  logout(){ 
    localStorage.removeItem('token');
    this.invalid= false; // 
  }
  constructor(private service:UsersService, private router:Router) { }

  ngOnInit() {
  	var value = localStorage.getItem('token');
  	
  	if (!value || value == undefined || value == "" || value.length == 0){		
  		this.router.navigate(['/']);  
  	}else{
      this.router.navigate(['/users']);  		
  	}
  }

}
