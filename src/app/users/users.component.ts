import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  // Variables	
  users;
  usersKeys = [];

  usersformupdate = new FormGroup({
    name: new FormControl('',Validators.required),
    phone: new FormControl('',Validators.required),
    id:new FormControl()
  });

  showSlim:Boolean = true;
  users;
  usersKeys = [];
  updates = [];
  lastOpenedToUpdate;
  fusers;

  showUpdate(key){
    if(this.updates[key]){
      this.updates[key] = false;
    }else{
      if(this.lastOpenedToUpdate){
        this.updates[this.lastOpenedToUpdate] = false;
      }
      this.updates[key] = true;
      this.usersformupdate.get('name').setValue(this.users[key].name);
      this.usersformupdate.get('phone').setValue(this.users[key].phone);
      this.usersformupdate.get('id').setValue(this.users[key].id);
      this.lastOpenedToUpdate = key;      
    } 
  }

  updateUser(id){
    if(this.usersformupdate.invalid) return;
    this.service.updateUser(this.usersformupdate.value).subscribe(response =>{
      console.log(response);
      this.service.getUsers().subscribe(response => {
        this.users =  response.json();
        this.usersKeys = Object.keys(this.users);

      });      
    });    
  }

  constructor(private service:UsersService, private router:Router) {  	
  	service.getUsers().subscribe(response=>{      
    	this.users = response.json();
    	this.usersKeys = Object.keys(this.users);
    	console.log(response.json());         
 	  });
  }

  deleteUser(key){    
    let index = this.usersKeys.indexOf(key);
    this.usersKeys.splice(index,1);
    this.service.deleteMessage(key).subscribe(
      response=> console.log(response));
  }

  optimisticAdd(name){
    var newKey =  this. usersKeys[this.usersKeys.length-1] +1;
    var newMessageObject ={};
    newMessageObject['name'] = name;
    this.users[newKey] = newMessageObject;
    this.usersKeys = Object.keys(this.users);
  }
  pasemisticAdd(){
  this.service.getUsers().subscribe(response=>{
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);      
    });      
  }  

  ngOnInit(){
    var value = localStorage.getItem('token');
    
    if(!value || value == undefined || value == "" || value.length == 0){    
      this.router.navigate(['/login']);
    }
  }

}
