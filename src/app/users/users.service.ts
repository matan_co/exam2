import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from '../../environments/environment';

@Injectable()
export class UsersService {
  // Variables
  http:Http;
  token;

  getUsers(){  
     let options =  {
        headers:new Headers({
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
          'Token': this.token    
        })
      }  

	   return this.http.get(environment.url + '/users', options);
  }

  postUser(data){
    let params = new HttpParams().append('name',data.name).append('phone',data.phone);      
    let options =  {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded',
        'Token': this.token        
      })
    }      
    return this.http.post(environment.url + '/users', params.toString(), options);
  }

  deleteMessage(key){  
    let options =  {
        headers:new Headers({
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
          'Token': this.token    
        })
      }

    return this.http.delete(environment.url + '/users/delete/' + key, options);
  }

  getUser(key){  
      let options =  {
        headers:new Headers({
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
          'Token': this.token    
        })
      }

     return this.http.get(environment.url + '/users/' + key,options);
  }

  updateUser(data){       
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded',
        'Token': this.token
      })   
    };

    let params = new HttpParams().append('name',data.name).append('phone',data.phone);  

    return this.http.put(environment.url + '/users/' + data.id,params.toString(), options);      
  }

  getUsersFireBase(){
    return this.db.list('/Users').valueChanges();
  }

  login(credentials){
     let options = {
        headers:new Headers({
         'content-type':'application/x-www-form-urlencoded'
        })
     }
    let  params = new HttpParams().append('name', credentials.user).append('password',credentials.password);
    return this.http.post('http://localhost:8080/login', params.toString(),options).map(response=>{ 
      let token = response.json().token;
      if (token){
        localStorage.setItem('token', token);
        console.log(token);
      }
   });
  }


  constructor(http:Http, private db:AngularFireDatabase) { 
 	  this.http = http; 
    this.token = localStorage.getItem('token');
  }

}
