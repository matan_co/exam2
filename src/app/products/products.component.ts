import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  	var value = localStorage.getItem('token');
    
    if(!value || value == undefined || value == "" || value.length == 0){    
      this.router.navigate(['/login']);
    }
  }

}
