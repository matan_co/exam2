// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
   	  production: false,
   	  url: 'http://localhost:8080',
	  firebase: {
	    apiKey: 'AIzaSyBwIl73XvNMySSwAQRjsFlfOigVHtwaSwI',
	    authDomain: 'users-9ac9e.firebaseapp.com',
	    databaseURL: 'https://users-9ac9e.firebaseio.com',
	    projectId: 'users-9ac9e',
	    storageBucket: 'users-9ac9e.appspot.com',
	    messagingSenderId: '562112452781'
	  }
};
